#!/bin/sh -x
path_on_server="`echo "$1" | perl -pe 's|^\w+:/*||g'`"

find_umphy_mountpoint () { 
    mount | PATH_ON_SERVER="$1" perl -ne '
        BEGIN { $found=0;$error=0; }
        # Find local mountpoint of Umphy server
        if (m|^\S*storage\.geo\.uni-tuebingen\.de(\S*)\s+on\s+(.*?)\s+type\s+cifs|g) {
            $mountpoint=$2;
            $serverpath=$1;
            # this would be the full path relative to the mountpoint 
            # if the toplevel ZDV share would have been mounted
            $p="/mn030116/DataAgBange/$ENV{PATH_ON_SERVER}"; 
            # print "p = $p   mountpoint = $mountpoint   serverpath = $serverpath\n"; 
            unless ($p=~s|^$serverpath||) {$error = 24;next;} # remove local mount nesting
            # print "p = $p   mountpoint = $mountpoint   serverpath = $serverpath\n";
            $localpath="$mountpoint/$p";
            # Remove double slashes
            $localpath=~s|/+|/|g;
            print "$localpath\n";
            $found++;
        }
        END { if((not $found and $error)){exit $error}; }
        '
}

ask_for_server_location() {
    zenity \
        --info \
        --width=400 \
        --text "You will now be asked to where you access the ZDV server.\n\nPlease select the directory containing Umphy/Exchange/etc..." || return 1
    zenity \
        --file-selection \
        --directory \
        --title "Please select the path to the server"
}


# Try user-configured location

user_config_file="$HOME/.config/open-umphy-server.conf"

get_user_configured_server_path () { 
    test -f "$user_config_file" || return 1
    cat "$user_config_file"
}

if user_configured_server_path="`get_user_configured_server_path`";then
    if test -e "$user_configured_server_path";then
        if test -e "$user_configured_server_path/$path_on_server";then
            xdg-open "$user_configured_server_path/$path_on_server" && exit
        else
            local_error="$local_error\n\nYour custom mountpoint '$user_configured_server_path' exists, but not the '$path_on_server' within."
        fi
    else
        local_error="$local_error\n\nYour custom mountpoint '$user_configured_server_path' does not exist."
    fi
fi

# Try mountpoints from manual mount or fstab

if ! local_paths="`find_umphy_mountpoint "$path_on_server"`";then
    local_error="$local_error\n\nYou seem to have mounted the Umphy server locally (e.g. via fstab), but too deeply to reach '$path_on_server'"
elif test -z "$local_paths";then
    local_error="$local_error\n\nNo local mountpoints (e.g. via fstab) for the Umphy server found."
else
    while IFS= read -r local_path;do
        if test -e "$local_path";then
            xdg_output="$xdg_output\n\n`xdg-open "$local_path"`" && exit
        else
            local_error="$local_error\n\nLocal path '$local_path' doesn't exist!"
        fi
    done < <(echo "$local_paths")
    local_error="$local_error\n\nCouldn't open any of these paths with your default file manager:

$local_paths

Error: $xdg_output

You might need to install a file manager and/or set one as default. For example for the Thunar file manager:

sudo pacman -Syu thunar

(or install it via your favourite GUI installer), and if launching this program still doesn't work afterwards, add/edit this to ~/.config/mimeapps.list:

[Default Applications]
inode/directory=thunar.desktop
"
fi


if zenity --question \
    --width 500 \
    --title "💥" \
    --ok-label "Specify Mountpoint" \
    --cancel-label "Mount in File Browser" \
    --text "
Couldn't open $path_on_server on the umphy server:

$local_error

What do you want to do?
";then
    # Ask for user-configured location
    if selection="`ask_for_server_location`";then
        test -n "$selection" || exit
        mkdir -p "`dirname "$user_config_file"`"
        echo "$selection" > "$user_config_file"
        xdg-open "$selection/$path_on_server"
    fi
    exit
fi

# Last resort: Try opening directly in file browser via samba url
# 
# Unfortunately this command doesn't exit with an error code when connection
# fails so we have to use this as a last resort as we can't figure out whether
# it worked...

xdg-open "smb://storage.geo.uni-tuebingen.de/mn030116/DataAgBange/$path_on_server"
