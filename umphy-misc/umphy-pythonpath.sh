# On our GitLab, packages are built for Arch Linux, which might already have the next version of Python
# This line tells every Python program to look for packages for all Python versions on the system
export PYTHONPATH="`for p in /usr/lib/python3*/site-packages/;do echo -n "$p:";done 2>/dev/null || true`"
