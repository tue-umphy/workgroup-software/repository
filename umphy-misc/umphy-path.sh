# Make sure ~/.local/bin is in PATH
(echo "$PATH" | tr ':' '\n' | grep -qx "$HOME/.local/bin") || export PATH="$HOME/.local/bin:$PATH"
