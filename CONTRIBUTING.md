# Contributing

## Updating AUR packages

```bash
# recurse into AUR submodules and update them
git submodule foreach sh -c 'if (git remote get-url origin | grep -q aur.archlinux.org);then git fetch; git checkout -f "$(git rev-list --all -n1)";fi'
# might want to commit now
```
